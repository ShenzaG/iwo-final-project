What is this repository for? 

This repository was created for my final research project for the course inleiding wettenschappelijk onderzoek.


What can you find in this repository?

- The shell script for my research project.
- The program get-gender.py which extracts the gender of a twitter user (based on their username).
- A text file with female Dutch first names.
- A text file with male Dutch first names.