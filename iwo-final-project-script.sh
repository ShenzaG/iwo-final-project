#!/bin/bash

# Stores all the tweets from the first week of March 2017 in the text file output1.txt.
for file in /net/corpora/twitter2/Tweets/2017/03/20170301*; do 
    zless "$file" | /net/corpora/twitter2/tools/tweet2tab -i user.name text >> output1.txt
done
for file in /net/corpora/twitter2/Tweets/2017/03/20170302*; do 
    zless "$file" | /net/corpora/twitter2/tools/tweet2tab -i user.name text >> output1.txt
done
for file in /net/corpora/twitter2/Tweets/2017/03/20170303*; do 
    zless "$file" | /net/corpora/twitter2/tools/tweet2tab -i user.name text >> output1.txt
done
for file in /net/corpora/twitter2/Tweets/2017/03/20170304*; do 
    zless "$file" | /net/corpora/twitter2/tools/tweet2tab -i user.name text >> output1.txt
done
for file in /net/corpora/twitter2/Tweets/2017/03/20170305*; do 
    zless "$file" | /net/corpora/twitter2/tools/tweet2tab -i user.name text >> output1.txt
done
for file in /net/corpora/twitter2/Tweets/2017/03/20170306*; do 
    zless "$file" | /net/corpora/twitter2/tools/tweet2tab -i user.name text >> output1.txt
done
for file in /net/corpora/twitter2/Tweets/2017/03/20170307*; do 
    zless "$file" | /net/corpora/twitter2/tools/tweet2tab -i user.name text >> output1.txt
done

# Stores only unique tweets and removes retweets.
sort output1.txt | uniq | grep -v -e 'RT' > output2.txt  

# Counts how many unique tweets there are without retweets.
echo "Amount of unique tweets without retweets:"
cat output2.txt | wc -l

# Counts how many tweets were written by female users.
echo "Amount of tweets written by female users:"
cat output2.txt | python3 get-gender.py | grep -w ^"female" | wc -l

# Counts how many tweets were written by male users.
echo "Amount of tweets written by male users:"
cat output2.txt | python3 get-gender.py | grep -w ^"male" | wc -l

# Stores the tweet that contain curse words in output3.txt.
grep -E 'shit|kut|fuck|godver|verdomme' output2.txt > output3.txt

# Counts how many tweets contain curse words.
echo "Amount of tweets that contain curse words:"
cat output3.txt | wc -l

# Counts the amount of tweets written by female users that contain curse words.
echo "Amount of tweets written by female users that contain curse words:"
cat output3.txt | python3 get-gender.py | grep -e ^"female" | wc -l

# Counts the amount of tweets written by male users that contain curse words.
echo "Amount of tweets written by male users that contain curse words:"
cat output3.txt | python3 get-gender.py | grep -e ^"male" | wc -l

 

